#% define beta_tag rc2
%define patchleveltag .0
%define baseversion 0.5
%bcond_with tests

Version: %{baseversion}%{patchleveltag}
Name: utshell
Summary: The utshell respect Bash
Release: 4%{?dist}
License: GPLv3+
Source0: utshell-%{baseversion}.tar.gz

# Official upstream patches
# Patches are converted to apply with '-p1'
Patch0001: 0001-fix-utshell-continue-help-countine.patch
Patch0002: 0002-fix-kill-l.patch
Patch0003: 0003-fix-exec-c-exec-l-Redundant-output.patch
Patch0004: 0004-fix-complete-p-no-display.patch
Patch0005: 0005-fix-translation-files-correlation.patch
Patch0006: 0006-fix-select-error.patch
Patch0007: 0007-fix-compile-error-on-riscv64.patch
Patch0008: 0008-fix-echo-a.z.patch
Patch0009: 0009-fix-type-return-error.patch
Patch0010: 0010-fix-coproc-command-error.patch
Patch0011: 0011-fix-arch-display.patch
# Other patches
# We don't want to add '/etc:/usr/etc' in standard utils path.

BuildRequires: gcc, cargo, rust
BuildRequires: texinfo bison
BuildRequires: ncurses-devel
BuildRequires: autoconf, gettext
BuildRequires: make
Requires: filesystem >= 3
#Provides: /bin/sh
Provides: /bin/utshell

ExclusiveArch: x86_64 aarch64 arm riscv64
%description
The GNU Bourne Again shell (Bash) is a shell or command language
interpreter that is compatible with the Bourne shell (sh). Bash
incorporates useful features from the Korn shell (ksh) and the C shell
(csh). Most sh scripts can be run by utshell without modification.


%prep
%autosetup -n %{name}-%{baseversion} -p1

# echo %{version} > _distribution
# echo %{release} > _patchlevel

# force refreshing the generated files
# rm y.tab.*

%build
autoconf
%configure --with-bash-malloc=no --with-afs

# Recycles pids is neccessary. When utshell 's last fork's pid was X
# and new fork's pid is also X, utshell has to wait for this same pid.
# Without Recycles pids utshell will not wait.
MFLAGS="CPPFLAGS=-D_GNU_SOURCE -DRECYCLES_PIDS -DDEFAULT_PATH_VALUE='\"/usr/local/bin:/usr/bin\"' `getconf LFS_CFLAGS`"

# work around missing deps in Makefiles
make "$MFLAGS" version.h
make "$MFLAGS" %{?_smp_mflags} -C builtins
make "$MFLAGS" %{?_smp_mflags}

%install
if [ -e autoconf ]; then
  # Yuck. We're using autoconf 2.1x.
  export PATH=.:$PATH
fi

%make_install install-headers

mkdir -p %{buildroot}/%{_sysconfdir}
mkdir -p %{buildroot}/%{_datadir}/utshell/resources/{zh-CN,en-US,zh-HK}
rm -f %{buildroot}%{_infodir}/dir
mkdir -p %{buildroot}%{_sysconfdir}/skel
LONG_BIT=$(getconf LONG_BIT)
mv %{buildroot}%{_bindir}/utshellbug \
   %{buildroot}%{_bindir}/utshellbug-"${LONG_BIT}"
ln -s utshellbug-"${LONG_BIT}" %{buildroot}%{_bindir}/utshellbug

%if %{with tests} 
%check
make check
%endif

# post is in lua so that we can run it without any external deps.  Helps
# for bootstrapping a new install.
# Jesse Keating 2009-01-29 (code from Ignacio Vazquez-Abrams)
# Roman Rakus 2011-11-07 (code from Sergey Romanov) #740611
%post -p <lua>
nl        = '\n'
sh        = '/bin/sh'..nl
utshell      = '/bin/utshell'..nl
f = io.open('/etc/shells', 'a+')
if f then
  local shells = nl..f:read('*all')..nl
  if not shells:find(nl..sh) then f:write(sh) end
  if not shells:find(nl..utshell) then f:write(utshell) end
  f:close()
end

%postun -p <lua>
-- Run it only if we are uninstalling
if arg[2] == 0
then
  t={}
  for line in io.lines("/etc/shells")
  do
    if line ~= "/bin/utshell" and line ~= "/bin/sh"
    then
      table.insert(t,line)
    end
  end

  f = io.open("/etc/shells", "w+")
  for n,line in pairs(t)
  do
    f:write(line.."\n")
  end
  f:close()
end

%files
#config(noreplace) /etc/skel/.b*
/usr/share/locale/*/LC_MESSAGES/utshell.mo
%{_bindir}/utshell
%license COPYING
%attr(0755,root,root) %{_bindir}/utshellbug[-.]*
%{_bindir}/utshellbug
%{_datadir}/utshell/resources/en-US/message.ftl
%{_datadir}/utshell/resources/zh-CN/message.ftl
%{_datadir}/utshell/resources/zh-HK/message.ftl
%{_libdir}/pkgconfig/utshell.pc
%{_includedir}/%{name}

%changelog
* Wed Mar 5 2025 wangmeng <wangmengc@uniontech.com> - 0.5.0-4
- feat: 修复coproc命令错误问题
- feat: 修复utshell --help 显示信息问题

* Fri Dec 13 2024 wangmeng <wangmengc@uniontech.com> - 0.5.0-3
- feat: add patch 0003-fix-echo-a.z.patch
- feat: add patch 0004-fix-type-return-error.patch
- 修复echo {a..z} 执行错误和type命令返回值错误

* Fri Jun 21 2024 wangmeng<wangmengc@uniontech.con> - 0.5.0-1
- fix:IA4DBK Change the verson number to be consistent with other software packages

* Thu May 23 2024 wangyaoyong <yaoyong.oerv@isrc.iscas.ac.cn> - 0.5.0-0.4
- fix:I9KZET riscv64 下cargo build编译时出现缺少依赖的问题

* Thu May 9 2024 wangmeng<wangmengc@uniontech.com> - 0.5.0-0.3
- fix:I9CANW select 关键字没有输出

* Thu Apr 11 2024 wangyaoyong <yaoyong.oerv@isrc.iscas.ac.cn> - 0.5.0-0.2
- update ExclusiveArch: riscv to riscv64

* Tue Apr 09 2024 wangmeng<wangmengc@uniontech.com> - 0.5.0-0.1
- fix:I9CC1Z trap --help 与trap -l 缺少部分提示 
- fix:I9CBXT help local中英文提示不符 
- fix:I9CBPP hash --help中英文提示不一致 
- fix:I9CBKZ exec -c exec -l存在多余输出 
- fix:I9CBG7 let --help 提示不全 
- fix:I9CAZZ ulimit -S命令不支持 
- fix:I9CAS8 kill -l序号有误 
- fix:I9CA9S continue --help 和countine 存在多余提示 
- fix:I9CBRG complete -p无输出

* Mon Apr 01 2024 wangmeng<wangmengc@uniontech.com> - 0.5.0
- update to version 0.5

* Tue Mar 26 2024 wangyaoyong <yaoyong.oerv@isrc.iscas.ac.cn> - 0.4.30411-0.02
- add ExclusiveArch: riscv

* Tue Apr 11 2023 Zhanghuanhuan<zhanghuanhuan@uniontech.com> - 0.4.30411-0.01
- fix:193533 utshell环境，Ctrl+D退出会话时，提示exit_builtin（刘彤）
- fix:193535 history -p mount和用例中预期不符（刘彤）
- fix:193581 utshell环境，未执行kill -CONT 恢复进程时，执行ctrl+C无法结束wait -f 进程（刘彤）
- fix:193557 declare 命令变量输出异常，和用例中预期不一致（张欢欢）

* Tue Mar 21 2023 Zhanghuanhuan<zhanghuanhuan@uniontech.com> - 0.4.30321
- fix : solve translation err (bugid :191483)

* Thu Mar 16 2023 Liutong<liutonga@uniontech.com> - 0.4.30316
- fix translation issue

* Tue Nov 01 2022 Liutong<liutonga@uniontech.com> - 0.4.14
- fix enable bug

* Tue Nov 01 2022 Liutong<liutonga@uniontech.com> - 0.4.13
- fix mem error, and modify mo

* Thu Oct 13 2022 Liutong<liutonga@uniontech.com> - 0.4.12
- change vendor, add po.

* Wed Oct 12 2022 Liutong<liutonga@uniontech.com> - 0.4.10
- add CARGO_HOME=${HOME}/.cargo

* Mon Oct 10 2022 Liutong<liutonga@uniontech.com> - 0.4.9
- rename project rash to utshell

* Sun Oct 9 2022 Zhanghuanhuan <zhanghuanhuan@uniontech.com> - 0.4.8
- fix: declare command issue  and history command  issue

* Thu Sep 29 2022 Wangmeng <wangmengc@uniontech.com> - 0.4.7
- fix: add help translation for all commands.

* Wed Sep 28 2022 Liutong <liutonga@uniontech.com> - 0.4.6
- fix: disown -r issue.

* Wed Sep 28 2022 Liutong <liutonga@uniontech.com> - 0.4.5
- fix: alias translation-core.

* Tue Sep 27 2022 Liutong <liutonga@uniontech.com> - 0.4.3
- fix: translation.
- fix: coredump in continue.
- fix: umask fmt error.
- fix: disown issue.
- add translation frame

* Mon Sep 19 2022 Liutong <liutonga@uniontech.com> - 0.4.2
- add loongarch64 arch and remove debug msg

* Wed Aug 31 2022 Liutong <liutonga@uniontech.com> - 0.4.1
- modify spec remove /usr/bin

* Fri Aug 26 2022 Zhanghuanhuan <zhanghuanhuan@uniontech.com> - 0.4.0
- build rash on aarch_64

